import logging
import yaml
from os import walk, path
from random import randrange
from typing import List, Optional

from hanabot.models.song import Song


class KaraokeProvider:
    _songs: List[Song] = []

    def __init__(self, fs_path: str):
        self.LOGGER = logging.getLogger('.'.join([
            self.__class__.__module__, self.__class__.__qualname__
        ]))
        self.path = fs_path
        self.refresh()

    def ensure_path_exists(self):
        if not path.exists(self.path):
            raise RuntimeError('%s: Path \'%s\' does not exist on disk' % (__class__.__name__, self.path))

    def refresh(self):
        self.ensure_path_exists()

        songlist = []

        for root, dirs, files in walk(self.path, topdown=True):
            for file in files:
                file = path.join(root, file)
                if not path.isfile(file):
                    continue
                if not file.endswith('.yaml'):
                    continue

                with open(file) as f:
                    doc = yaml.safe_load(f)

                if 'songs' not in doc:
                    self.LOGGER.error('Invalid yaml document: %s' % file)
                    continue

                for song in doc['songs']:
                    entry = Song(
                        song['filename'] if 'filename' in song else None,
                        doc['name'] if 'name' in doc else None,
                        doc['date'] if 'date' in doc else None,
                        song['name'] if 'name' in song else None,
                        song['artist'] if 'artist' in song else None,
                        song['timestamp'] if 'timestamp' in song else None,
                        song['notes'] if 'notes' in song else None
                    )

                    if not entry.filename:
                        self.LOGGER.error('Broken entry: %s@%s' % (file, doc['songs'].index(song)))
                        continue

                    songfile = path.join(root, entry.filename)
                    if path.exists(songfile) and path.isfile(songfile):
                        entry.filename = songfile
                        songlist.append(entry)
                        self.LOGGER.info('Adding song %s' % entry.filename)
                    else:
                        self.LOGGER.warning('Song doesn\'t exist: %s' % songfile)

        for song in songlist:
            self.LOGGER.info('%s %s' % (songlist.index(song), song.filename))

        self._songs = songlist

    def list_get(self) -> List[Song]:
        return self._songs

    def random_get(self) -> Optional[Song]:
        if not len(self._songs):
            return None

        idx = randrange(len(self._songs))
        return self._songs[idx]
