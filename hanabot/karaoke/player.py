from discord import VoiceClient, Client, TextChannel, FFmpegPCMAudio, Embed
from random import shuffle
from typing import List, Optional

from hanabot.karaoke.provider import KaraokeProvider
from hanabot.models import SongRequest, Song


class KaraokePlayer:
    _request_list: List[SongRequest] = []
    _autoqueue_list: List[Song] = []
    _terminated = False

    def __init__(self, voice_client: VoiceClient, client: Client, command_channel: TextChannel, karaoke_provider: KaraokeProvider):
        self.voice_channel = voice_client
        self.discord_client = client
        self.command_channel = command_channel
        self.karaoke_provider = karaoke_provider

    async def autoplay_start(self):
        await self.autoplay_next()

    def autoplay_stop(self):
        self.voice_channel.stop()

    async def autoplay_next(self):
        if self._terminated:
            if self.voice_channel.is_connected():
                await self.voice_channel.disconnect()
            return

        if not self.voice_channel.is_connected():
            return

        if len(self._request_list):
            request = self._request_list[0]
            song = request.song
            del self._request_list[0]
        else:
            self._autoqueue_ensure_length()
            if len(self._autoqueue_list) == 0:
                await self.command_channel.send('Karaoke: Failed to get the next item to play')
                return

            song = self._autoqueue_list[0]
            del self._autoqueue_list[0]

        audio = FFmpegPCMAudio(song.filename)
        if self.voice_channel.is_playing():
            self.voice_channel.stop()

        self.voice_channel.play(audio, after=lambda x: self.voice_channel.is_playing() or self.discord_client.loop.create_task(self.autoplay_next()))

        embed = Embed(title='Now singing')
        embed.description = '**Title**: %s\n**Artist**: %s' % (song.name, song.artist)
        if song.stream_date:
            embed.add_field(name='Stream date', value=song.stream_date)
        if song.stream_name:
            embed.add_field(name='Stream name', value=song.stream_name)
        if song.timestamp:
            embed.add_field(name='Stream timestamp', value=song.timestamp)
        if song.notes:
            embed.description += '\n**Note**: %s' % song.notes
        await self.command_channel.send(embed=embed)

    def _autoplay_get_item(self) -> Optional[Song]:
        return self.karaoke_provider.random_get()

    def add_request(self, index):
        self._request_list.append(SongRequest(
            index,
            self.karaoke_provider.list_get()[index]
        ))

    def refresh_song_lists(self):
        new_list: List[SongRequest] = []

        for request in self._request_list:
            new_object = [x for x in self.karaoke_provider.list_get() if x.filename == request.song.filename]
            if len(new_object):
                new_list.append(
                    SongRequest(
                        self.karaoke_provider.list_get().index(new_object[0]),
                        new_object[0],
                    ),
                )
        self._request_list = new_list

        new_list2: List[Song] = []
        for song in self._autoqueue_list:
            new_object = [x for x in self.karaoke_provider.list_get() if x.filename == song.filename]
            if len(new_object):
                new_list2.append(new_object[0])
        self._autoqueue_list = new_list2

    def get_requests(self):
        return self._request_list

    def get_autoqueue(self):
        return self._autoqueue_list

    def flush_requests(self):
        self._request_list = []

    def _autoqueue_ensure_length(self):
        if len(self._autoqueue_list) >= len(self.karaoke_provider.list_get()) / 4:
            return

        items = list(self.karaoke_provider.list_get())
        shuffle(items)
        self._autoqueue_list += items

    async def terminate(self):
        self._terminated = True
        self.autoplay_stop()
        self.voice_channel.stop()
        await self.voice_channel.disconnect()
