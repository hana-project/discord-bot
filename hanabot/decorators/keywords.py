from functools import wraps
from typing import List

from discord import Message
from eris.decorators import BaseDecorator
from eris.events.hooks import HOOK_EAT_NONE
from eris.events.types import EventBase


class KeywordPrecondition(BaseDecorator):
    def __init__(self, keywords: List[str]):
        self.keywords = keywords

    def __call__(self, func):
        @wraps(func)
        async def wrapped_f(*args, **kwargs):
            event: EventBase = args[self._EVENT_OFFSET]
            actual: Message = event.actual
            splits = actual.content.split(' ')

            if splits[0] != '' and splits[0] in self.keywords:
                return await func(*args, **kwargs)

            return HOOK_EAT_NONE

        return wrapped_f
