import logging
from eris.decorators.admin import AdminOnly
from eris.decorators.hook import Hook
from eris.events.hooks import HOOK_EAT_NONE
from eris.events.types.eventbase import EventBase
from eris.modules.base import ModuleBase
from discord import Message, VoiceChannel, VoiceClient, Embed, ClientException, Forbidden, Guild
from typing import Dict, List
from asyncio.exceptions import TimeoutError as AsyncTimeoutError

from hanabot.karaoke.player import KaraokePlayer
from hanabot.karaoke.provider import KaraokeProvider
from hanabot.models import Song
from hanabot.decorators.keywords import KeywordPrecondition

logging.basicConfig()
LOGGER = logging.getLogger(__name__)


class KaraokeModule(ModuleBase):
    players: Dict[int, KaraokePlayer] = {}
    karaoke_provider: KaraokeProvider

    def register(self):
        karaoke_song_dir = self.client.config.karaoke.dir
        self.karaoke_provider = KaraokeProvider(karaoke_song_dir)

    def unregister(self):
        for guild_id in list(self.players.keys()):
            self.client.loop.create_task(
                self.players[guild_id].command_channel.send('Stopping the karaoke session as the bot is restarting')
            )
            self.client.loop.create_task(
                self._terminate_session(guild_id)
            )

    @KeywordPrecondition(keywords=['!join', '!j'])
    @Hook(name='Start a karaoke session', event_type='message')
    async def handle_karaoke_join(self, event: EventBase):
        actual: Message = event.actual

        if not actual.guild:
            await actual.channel.send('This bot only supports karaoke sessions on servers')
        elif actual.author.voice and actual.author.voice.channel:
            channel: VoiceChannel = actual.author.voice.channel

            if actual.guild.id in self.players:
                player: KaraokePlayer = self.players[actual.guild.id]

                # We're doing this as an access-check. If we cannot send it, we get an exception instead
                try:
                    await actual.channel.send('Moving the existing karaoke session')
                except Forbidden:
                    self._log_message_failed(actual)
                    return HOOK_EAT_NONE

                if player.command_channel != actual.channel:
                    player.command_channel = actual.channel
                if player.voice_channel.channel != channel:
                    await player.voice_channel.move_to(channel)
            else:
                # We're doing this as an access-check. If we cannot send it, we get an exception instead
                try:
                    await actual.channel.send('Starting a new karaoke session')
                except Forbidden:
                    self._log_message_failed(actual)
                    return HOOK_EAT_NONE

                try:
                    voiceclient: VoiceClient = await channel.connect(timeout=5.0)
                except ClientException:
                    # Already connected but I don't have a connection tracked. Try to recover the connection
                    voiceclients: List[VoiceClient] = [x for x in self.client.voice_clients if isinstance(x, VoiceClient)]
                    voiceclients = [x for x in voiceclients if x.guild.id == actual.guild.id]
                    if not len(voiceclients):
                        self.LOGGER.info('ClientException: unable to recover for guild %s' % actual.guild.id)
                        await actual.channel.send('Unable to connect to the voice channel.')
                        return HOOK_EAT_NONE

                    voiceclient: VoiceClient = voiceclients[0]
                    self.LOGGER.info('Recovered stray voice connection for guild %s' % voiceclient.guild.id)
                except AsyncTimeoutError:
                    self.LOGGER.info('Unable to conect to VC (Timeout) for guild %s' % actual.guild.id)
                    await actual.channel.send('Unable to connect to the voice channel. Are you in a voice channel to which I have access to?')
                    return HOOK_EAT_NONE

                karaoke = KaraokePlayer(voiceclient, self.client, actual.channel, self.karaoke_provider)
                self.LOGGER.info('Starting karaoke session for guild %s' % (actual.guild.id))
                self.players[actual.guild.id] = karaoke
                await karaoke.autoplay_start()
        else:
            try:
                await actual.channel.send('Please join a voice channel fist')
            except Forbidden:
                self._log_message_failed(actual)
                pass

        return HOOK_EAT_NONE

    @KeywordPrecondition(keywords=['!part', '!yeet'])
    @Hook(name='Stop the karaoke session', event_type='message')
    async def handle_karaoke_part(self, event: EventBase):
        actual: Message = event.actual

        if not actual.guild:
            await actual.channel.send('This bot only supports karaoke sessions on servers')
        elif actual.author.voice and actual.author.voice.channel:
            if actual.guild.id in self.players:
                await self._terminate_session(actual.guild.id)

                await event.actual.channel.send('Karaoke session stopped')
                return HOOK_EAT_NONE

            await event.actual.channel.send('No karaoke sessions ongoing in this server')
        else:
            await event.actual.channel.send('Please join a voice channel fist')

        return HOOK_EAT_NONE

    @KeywordPrecondition(keywords=['!next', '!skip'])
    @Hook(name='Skip the current song', event_type='message')
    async def handle_karaoke_next(self, event: EventBase):
        actual: Message = event.actual

        if not actual.guild:
            await actual.channel.send('This bot only supports karaoke sessions on servers')
        elif actual.author.voice and actual.author.voice.channel:
            if actual.guild.id in self.players:
                self.LOGGER.info('Next song in karaoke session for guild %s' % (actual.guild.id))
                karaoke: KaraokePlayer = self.players[actual.guild.id]
                await karaoke.autoplay_next()
            else:
                await event.actual.channel.send('No karaoke sessions ongoing in this server')
        else:
            await event.actual.channel.send('Please join a voice channel fist')

        return HOOK_EAT_NONE

    @AdminOnly()
    @KeywordPrecondition(keywords=['!reload'])
    @Hook(name='Reload the song list', event_type='message')
    async def handle_karaoke_reload(self, event: EventBase):
        num_songs_old = len(self.karaoke_provider.list_get())
        self.karaoke_provider.refresh()
        num_songs_new = len(self.karaoke_provider.list_get())

        for player in self.players.values():
            player.refresh_song_lists()

        await event.actual.channel.send('Reloaded. Previous songcount: %s, New songcount %s.' % (num_songs_old, num_songs_new))

        return HOOK_EAT_NONE

    @AdminOnly()
    @KeywordPrecondition(keywords=['!debug'])
    @Hook(name='Debug', event_type='message')
    async def handle_karaoke_reload(self, event: EventBase):
        print(self.players)

        await event.actual.channel.send('OK')

        return HOOK_EAT_NONE

    @AdminOnly()
    @KeywordPrecondition(keywords=['!dc'])
    @Hook(name='Force Disconnect', event_type='message')
    async def handle_karaoke_reload(self, event: EventBase):
        actual: Message = event.actual
        for client in self.client.voice_clients:
            vc: VoiceClient = client
            guild = vc.guild
            if not isinstance(guild, Guild):
                continue

            if guild.id == actual.guild.id:
                print("Forcibly disconnecting session for guild #%s" % actual.guild.id)
                await vc.disconnect()

        if actual.guild.id in self.players:
            print("Terminating player for guild #%s" % actual.guild.id)
            await self.players[actual.guild.id].terminate()
            del self.players[actual.guild.id]

        print("Forcibly terminated")
        await actual.channel.send('Forcibly terminated')
        return HOOK_EAT_NONE

    @KeywordPrecondition(keywords=['!search'])
    @Hook(name='Karaoke search song', event_type='message')
    async def handle_search_song(self, event: EventBase):
        actual: Message = event.actual
        splits = actual.content.split(' ')

        if len(splits) == 1:
            await actual.channel.send('At least one keyword is required')
            return HOOK_EAT_NONE

        result = self.karaoke_provider.list_get()
        for split in splits[1:]:
            split = split.lower()
            result = [
                item for item in result
                if split in str(item.name or '').lower() or split in str(item.artist or '').lower()
            ]

        if not result:
            await actual.channel.send('No matches found')
            return HOOK_EAT_NONE

        embed = Embed(title='Search results')
        embed.description = '\n'.join([
            '%s: **%s** - %s (%s)' % (
                self.karaoke_provider.list_get().index(item), item.name, item.artist, item.stream_date
            ) for item in result[:10]
        ])
        await actual.channel.send(embed=embed)

    @KeywordPrecondition(keywords=['!queue', '!q'])
    @Hook(name='Karaoke current queue', event_type='message')
    async def handle_karaoke_queue(self, event: EventBase):
        actual: Message = event.actual
        splits = actual.content.split(' ')

        if not actual.guild:
            await actual.channel.send('This bot only supports karaoke sessions on servers')
            return HOOK_EAT_NONE

        if actual.guild.id not in self.players:
            await event.actual.channel.send('No karaoke sessions ongoing in this server')
            return HOOK_EAT_NONE

        player = self.players[actual.guild.id]

        if len(splits) == 1:
            embed = Embed(title='Queue list')
            requested: List[Song] = [x.song for x in player.get_requests()]
            autoqueued: List[Song] = [x for x in player.get_autoqueue()]
            embed.description = ''
            if requested:
                embed.description += '__**Requested**__\n\n' + '\n'.join([
                    '**%s** - %s (%s)' % (
                        song.name,
                        song.artist,
                        song.stream_date
                    ) for song in requested[:10]
                ])
            if autoqueued and len(requested) < 10:
                if len(requested):
                    embed.description += '\n\n'
                autoqueued = autoqueued[:10 - len(requested)]
                embed.description += '__**Autoqueue**__\n\n' + '\n'.join([
                    '**%s** - %s (%s)' % (
                        song.name,
                        song.artist,
                        song.stream_date
                    ) for song in autoqueued
                ])
            await event.actual.channel.send(embed=embed)
            return HOOK_EAT_NONE

        try:
            indexes = [int(x) for x in splits[1:]]
        except ValueError:
            await event.actual.channel.send('One or more requested songs are not a number')
            return HOOK_EAT_NONE

        for index in indexes:
            if index < len(self.karaoke_provider.list_get()):
                player.add_request(index)

        await event.actual.channel.send('Request(s) enqueue\'d')
        return HOOK_EAT_NONE

    @KeywordPrecondition(keywords=['!flush'])
    @Hook(name='Karaoke current queue flush', event_type='message')
    async def handle_karaoke_queue_flush(self, event: EventBase):
        actual: Message = event.actual

        if not actual.guild:
            await actual.channel.send('This bot only supports karaoke sessions on servers')
            return HOOK_EAT_NONE

        if actual.guild.id not in self.players:
            await event.actual.channel.send('No karaoke sessions ongoing in this server')
            return HOOK_EAT_NONE

        player = self.players[actual.guild.id]
        player.flush_requests()

        await event.actual.channel.send('All requests removed')
        return HOOK_EAT_NONE

    @KeywordPrecondition(keywords=['!stats'])
    @Hook(name='Karaoke stats', event_type='message')
    async def handle_stats(self, event: EventBase):
        actual: Message = event.actual
        splits = actual.content.split(' ')

        if len(splits) == 1:
            await actual.channel.send('**Available stats**\n`!stats stream`: Number of songs per stream')
            return HOOK_EAT_NONE

        embed = None
        if splits[1] == 'stream':
            songs = self.karaoke_provider.list_get()
            stats = {}

            for song in songs:
                if song.stream_date:
                    label = '%s (%s)' % (song.stream_name, song.stream_date)
                else:
                    label = song.stream_name

                if label not in stats:
                    stats[label] = 1
                else:
                    stats[label] += 1

            stats = sorted(stats.items(), key=lambda x: x[1], reverse=True)

            embed = Embed(title='Stream stats')
            embed.description = '\n'.join([
                '%s: %s' % (stream, count)
                for (stream, count) in stats
            ])

        if embed is not None:
            await actual.channel.send(embed=embed)

        return HOOK_EAT_NONE

    @Hook(name='Karaoke user voicestate changed', event_type='voicestate')
    async def handle_voicestate_changed(self, event: EventBase):
        # Check all ongoing sessions
        for guild_id in list(self.players.keys()):
            session: KaraokePlayer = self.players[guild_id]
            if len(session.voice_channel.channel.members) == 1:
                await self._terminate_session(guild_id)
                await session.command_channel.send('Stopping the karaoke session as everyone left the voice channel')

    async def _terminate_session(self, guild_id):
        self.LOGGER.info('Stopping karaoke session for guild %s' % (guild_id))
        karaoke: KaraokePlayer = self.players[guild_id]
        await karaoke.terminate()
        del self.players[guild_id]

    def _log_message_failed(self, actual: Message):
        self.LOGGER.info('Cannot send a message to guild %s (%s) ch %s (%s)' % (
            actual.guild.name, actual.guild.id, actual.channel.name, actual.channel.id
        ))
