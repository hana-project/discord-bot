from dataclasses import dataclass


@dataclass
class Song:
    filename: str
    stream_name: str
    stream_date: str
    name: str
    artist: str
    timestamp: str
    notes: str
