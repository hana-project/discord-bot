from dataclasses import dataclass
from .song import Song


@dataclass
class SongRequest:
    index: int
    song: Song
