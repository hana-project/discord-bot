from eris.config import Config

from pydantic import Field, BaseModel


class KaraokeConfig(BaseModel):
    dir: str


class HanaConfig(Config):
    karaoke: KaraokeConfig = Field(alias='hanabot.modules.karaoke')
