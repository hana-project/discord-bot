#!/usr/bin/env python3
import logging
import time
import yaml
from aiohttp import ClientConnectorError
from eris.core import Core

from hanabot.models.config import HanaConfig

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)
logging.getLogger('hanabot').setLevel(logging.DEBUG)
logging.getLogger('eris').setLevel(logging.DEBUG)


def main():
    while True:
        try:
            with open('config.yaml') as f:
                config = yaml.load(f, Loader=yaml.SafeLoader)
            core = Core(config, config_cls=HanaConfig)
            core.run()
        except ClientConnectorError as exception:
            LOGGER.exception(exception)
            time.sleep(5)


if __name__ == '__main__':
    LOGGER.info("Starting up...")
    main()
